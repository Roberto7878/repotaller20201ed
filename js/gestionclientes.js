var clientesObtenidos;

function getClientes() {
 var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
 var request = new XMLHttpRequest();

 request.onreadystatechange = function () {
   if (this.readyState == 4 && this.status == 200) {
    clientesObtenidos =request.responseText;
    procesarClientes();
   }
 }
 request.open("GET", url, true);
 request.send();
}

 function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");


var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    // tr=tablerow, td=tabledata
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
   columnaCiudad.innerText = JSONClientes.value[i].City;

   var columnaBandera = document.createElement("td");
   var imgBandera = document.createElement("img");

   if (JSONClientes.value[i].Country == "UK") {
   imgBandera.src = rutaBandera + "United-Kingdom.png";
   } else {
   imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
   }

   imgBandera.style.width = "70px";
   imgBandera.style.height = "40px";
   columnaBandera.appendChild(imgBandera);

   nuevaFila.appendChild(columnaNombre);
   nuevaFila.appendChild(columnaCiudad);
   nuevaFila.appendChild(columnaBandera);

   tbody.appendChild(nuevaFila);
   }

   tabla.appendChild(tbody);
   divTabla.appendChild(tabla);
    }
